from django.db import models

class Tutorials(models.Model):
    title = models.CharField(max_length=50)
    reference_video = models.CharField(max_length=2048) # link of tutorial video
    
    def __str__(self):
        return self.title
    