from rest_framework import generics, permissions

from .serializers import TutorialsSerializer
from .models import Tutorials

class TutorialsList(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Tutorials.objects.all()
    serializer_class = TutorialsSerializer
