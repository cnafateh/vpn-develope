from rest_framework import generics

from .serializers import HomePageSerializer
from .models import HomePageDatas

class HomePageList(generics.ListAPIView):
    queryset = HomePageDatas.objects.all()
    serializer_class = HomePageSerializer
