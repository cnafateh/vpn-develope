from django.db import models

class HomePageDatas(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False)
    header = models.CharField(max_length=255, null=False, blank=False)
    body = models.TextField()

    def __str__(self):
        return self.title