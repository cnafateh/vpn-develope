from rest_framework import serializers

from .models import HomePageDatas

class HomePageSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("header", "body",)
        model = HomePageDatas
