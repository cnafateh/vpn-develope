from django.db import models
from plans.models import Plans
from django.conf import settings

class UserOrder(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,)
    plan = models.ForeignKey(Plans, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.user} - {self.plan.category} - {self.plan.period} days"
