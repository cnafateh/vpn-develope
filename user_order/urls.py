from django.urls import path
from .views import UserOrderApi

urlpatterns = [
    path("", UserOrderApi.as_view(), name="plans"),
]


