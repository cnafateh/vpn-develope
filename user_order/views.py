from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import UserOrderSerializer
from .models import UserOrder


class UserOrderApi(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = UserOrder.objects.all()
    serializer_class = UserOrderSerializer

    def post(self, request):
        serializer = UserOrderSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(user=request.user)
            return Response(serializer.data, status=200)
        else:
            return Response({"errors": serializer.errors}, status=400)
        
    
