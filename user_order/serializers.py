from rest_framework import serializers


from .models import UserOrder

class UserOrderSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("plan",)
        model = UserOrder
