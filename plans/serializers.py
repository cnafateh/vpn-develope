from rest_framework import serializers

from .models import Plans

class PlansSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("__all__")
        model = Plans

