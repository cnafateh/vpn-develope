from rest_framework import generics

from .serializers import PlansSerializer
from .models import Plans


class PlansList(generics.ListAPIView):
    queryset = Plans.objects.all()
    serializer_class = PlansSerializer

