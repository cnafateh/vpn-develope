from django.urls import path
from .views import PlansList

urlpatterns = [
    path("", PlansList.as_view(), name="plans"),
]
