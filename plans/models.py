from django.db import models

categories = (
    ("BasicPlan", "BasicPlan"),
    ("IpSabet", "IpSabet"),
)

class Plans(models.Model):
    category = models.CharField(max_length=255, choices=categories, null=False, blank=False)
    period = models.IntegerField()
    price = models.IntegerField()
    user_count = models.IntegerField()
    instant_delivery = models.BooleanField(default=True)
    kill_switch = models.BooleanField(default=True)
    all_os = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.category} - {self.period} days"
    



# class BasicPlan(models.Model):
#     period = models.IntegerField()
#     price = models.IntegerField()
#     user_count = models.IntegerField()
#     instant_delivery = models.BooleanField(default=True)
#     all_os = models.BooleanField(default=True)

#     def __str__(self):
#         return f"{self.period} days {self.price} T"
    


# class IpSabet(models.Model):
#     period = models.IntegerField()
#     price = models.IntegerField()
#     user_count = models.IntegerField()
#     instant_delivery = models.BooleanField(default=True)
#     all_os = models.BooleanField(default=True)
#     kill_switch = models.BooleanField(default=True)
#     ip_extension = models.BooleanField(default=True)

#     def __str__(self):
#         return f"{self.period} days {self.price} T"