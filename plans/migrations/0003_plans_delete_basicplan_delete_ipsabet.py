# Generated by Django 5.0.4 on 2024-04-10 21:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plans', '0002_ipsabet'),
    ]

    operations = [
        migrations.CreateModel(
            name='Plans',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.CharField(choices=[('BasicPlan', 'BasicPlan'), ('IpSabet', 'IpSabet')], max_length=255)),
                ('period', models.IntegerField()),
                ('price', models.IntegerField()),
                ('user_count', models.IntegerField()),
                ('instant_delivery', models.BooleanField(default=True)),
                ('kill_switch', models.BooleanField(default=True)),
                ('all_os', models.BooleanField(default=True)),
            ],
        ),
        migrations.DeleteModel(
            name='BasicPlan',
        ),
        migrations.DeleteModel(
            name='IpSabet',
        ),
    ]
