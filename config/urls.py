from django.contrib import admin
from django.urls import path, include

from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView


urlpatterns = [
    path('admin/', admin.site.urls),
    path("api/plans/", include("plans.urls")),
    path("api/home/", include("home.urls")),
    path("api/user_order/", include("user_order.urls")),
    path("api/tutorials/", include("tutorials.urls")),

    path("api/auth/", include("rest_framework.urls")),
    path("api/rest_auth/", include("dj_rest_auth.urls")),
    path("api/rest_auth/registration/", include("dj_rest_auth.registration.urls")),
]

urlpatterns += [
    # YOUR PATTERNS
    path('api/schema/', SpectacularAPIView.as_view(), name='schema'),
    # Optional UI:
    path('api/schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('api/schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),
]